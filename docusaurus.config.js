const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

const math = require('remark-math');
const katex = require('rehype-katex');

/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Coldwire',
  tagline: '',
  url: 'https://doc.coldwire.org',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.png',
  organizationName: 'coldwire',
  projectName: 'coldwire',
  themeConfig: {
    navbar: {
      title: 'Documentation',
      logo: {
        alt: 'Coldwire logo',
        src: 'img/logo.png',
      },
      items: [
        {
          href: 'https://codeberg.org/coldwire',
          label: 'Codeberg',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Community',
          items: [
            {
              label: 'Element',
              href: 'https://organize.coldwire.org',
            },
            {
              label: 'Twitter',
              href: 'https://twitter.com/coldwirehq',
            },
          ],
        },
      ],
      copyright: `Copyleft 🄯 Your autonomous collective <3`,
    },
    prism: {
      theme: lightCodeTheme,
      darkTheme: darkCodeTheme,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',      
      {
        docs: {
          remarkPlugins: [math],
          rehypePlugins: [katex],
          sidebarPath: require.resolve('./sidebars.js'),
          routeBasePath: '/',
          editUrl:
          'https://codeberg.org/coldwire/documentation/src/branch/main/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
    [
      'redocusaurus',
      {
        debug: Boolean(process.env.DEBUG || process.env.CI),
        theme: {
          primaryColor: '#1890ff',
          redocOptions: { hideDownloadButton: false },
        },
      },
    ]
  ]
};
