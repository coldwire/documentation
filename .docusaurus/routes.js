
import React from 'react';
import ComponentCreator from '@docusaurus/ComponentCreator';

export default [
  {
    path: '/__docusaurus/debug',
    component: ComponentCreator('/__docusaurus/debug','7a8'),
    exact: true
  },
  {
    path: '/__docusaurus/debug/config',
    component: ComponentCreator('/__docusaurus/debug/config','6f0'),
    exact: true
  },
  {
    path: '/__docusaurus/debug/content',
    component: ComponentCreator('/__docusaurus/debug/content','2e0'),
    exact: true
  },
  {
    path: '/__docusaurus/debug/globalData',
    component: ComponentCreator('/__docusaurus/debug/globalData','82f'),
    exact: true
  },
  {
    path: '/__docusaurus/debug/metadata',
    component: ComponentCreator('/__docusaurus/debug/metadata','016'),
    exact: true
  },
  {
    path: '/__docusaurus/debug/registry',
    component: ComponentCreator('/__docusaurus/debug/registry','298'),
    exact: true
  },
  {
    path: '/__docusaurus/debug/routes',
    component: ComponentCreator('/__docusaurus/debug/routes','492'),
    exact: true
  },
  {
    path: '/',
    component: ComponentCreator('/','149'),
    routes: [
      {
        path: '/',
        component: ComponentCreator('/','96c'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/Bloc documentation/deployment/bare-metal',
        component: ComponentCreator('/Bloc documentation/deployment/bare-metal','071'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/Bloc documentation/deployment/intro',
        component: ComponentCreator('/Bloc documentation/deployment/intro','ee6'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/Bloc documentation/deployment/nomad-cluster',
        component: ComponentCreator('/Bloc documentation/deployment/nomad-cluster','246'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/Bloc documentation/deployment/with-docker',
        component: ComponentCreator('/Bloc documentation/deployment/with-docker','ce1'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/Bloc documentation/development/api',
        component: ComponentCreator('/Bloc documentation/development/api','4c6'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/Bloc documentation/development/intro',
        component: ComponentCreator('/Bloc documentation/development/intro','d47'),
        exact: true,
        sidebar: "tutorialSidebar"
      }
    ]
  },
  {
    path: '*',
    component: ComponentCreator('*')
  }
];
