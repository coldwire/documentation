---
sidebar_position: 1
title: Introduction
slug: /
---

# Coldwire Documentation

Welcome to Coldwire's documentation. Everything you need to contribute to Coldwire, run your own services instances, join the project, or to learn more about it.