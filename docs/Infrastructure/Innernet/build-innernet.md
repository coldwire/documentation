---
sidebar_position: 1
title: Build innernet for ARM64
---

### Download innernet source code
```sh
git clone https://github.com/tonarino/innernet.git
cd innernet
```

### Setup cross
#### Install cross
```sh
cargo install -f cross
```

#### Setup config file
*Cross.toml* >
```toml
[target.aarch64-unknown-linux-gnu]
image = "innarm:v1"
```

### Setup custom docker file for libsqlite
*Dockerfile* >
```Dockerfile
FROM ghcr.io/cross-rs/aarch64-unknown-linux-gnu:edge

RUN dpkg --add-architecture arm64 && \
    apt-get update && \
    apt-get install --assume-yes libsqlite3-dev:arm64
```

### Build
```sh
cross build --target aarch64-unknown-linux-gnu
```